<- [Return](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/00_get-started.md)

# 2. Modifying placeholders

The program uses placeholders to automatically fill data for the repository files (user name, email, github etc.).

**For example:** If you use the placeholder $user_name within a template file, it will automatically fill "Monty Python" in the generated repository file.

You can modify and add placeholders as you wish.

The placeholders are located in data/settings/placeholders.json:

	{
		"$user_name": "Monty Python",
		"$user_handle": "montypython",
		"$user_email": "montypython@email.com",
		"$user_github": "https://github.com/montypython"
	}

## Example

If we have the following content in a template file:

	# $repo_name
	
	Project created by $user_name.

	$repo_description

When we generate the new project repository, the file will look like this:

	# My Project Name
	
	Project created by Monty Python.
	
	This is the description of the project repository.

## Repository placeholders

There's few placeholders that you can use concerning the repository:

	$repo_name
	$repo_description
	$repo_license
