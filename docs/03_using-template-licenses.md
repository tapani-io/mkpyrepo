<- [Return](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/00_get-started.md)

# 3. Using template licenses

You can automatically generate a license for new repositories. The license templates are located in data/licenses/ directory.

You can add a template license by adding a file in the license templates directory (.txt or .md file). Use placeholders like $year and $repo_name within the license to automatically fill in data about the repository.

MIT license is already included in the templates.
