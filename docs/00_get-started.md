# Get Started

Get started guide for mkpyrepo. I hope this guide helps you understand the functionality of the program in more detail.

## Table of contents

1. [Using template structures](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/01_using-template-structures.md)
2. [Modifying placeholders](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/02_modifying-placeholders.md)
3. [Using template licenses](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/03_using-template-licenses.md)

## Usage

**1. Clone the repository:**

	git clone https://gitlab.com/tapani-io/mkpyrepo.git

**2. Change user settings:**

	Change user settings in data/settings/placeholders.json

**3. Run program:**

	python3 mkpyrepo/mkpyrepo.py

**4. Enter details:**

	Repo name: My Repo
	Short description: This is my awesome new repo.
	License: MIT
	Template: default

*NOTE: description, license and template fields can be left empty. If license is empty, then an empty license is generated. If template is empty, the default structure will be used.
