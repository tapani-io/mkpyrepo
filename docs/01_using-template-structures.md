<- [Return](https://gitlab.com/tapani-io/mkpyrepo/blob/master/docs/00_get-started.md)

# 1. Using template structures

The program uses template structures to generate the new repository file structure. The templates are located in data/templates/ directory.

## Adding a new structure

You can add a new template structure by adding a new directory (data/templates/new-template/). All files and sub-directories you add in the directory will be used for new repositories you create.

***NOTE:** The program will automatically rename a directory called "main" and "main.py" file with repository name, which is common practice.*

There is a default template already with a basic commonly used structure.

```bash
├── templates
    └── default
    	├── main
    	│	├── __init__.py
    	│	└── main.py
    	├── AUTHORS.md
    	├── CONTRIBUTING.md
    	├── LICENSE
    	├── README.md
    	├── requirements.txt
    	└── .gitignore
```
