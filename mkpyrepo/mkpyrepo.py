#!/usr/bin/env python3
#
# ===========================================================
# 
# mkpyrepo:
#
# Automatically generate a skeleton file hierarchy structure
# for a new Python project repository.
#
# Author: Tapani Io
# Email: tapani.io@protonmail.com
# GitLab: https://gitlab.com/tapani-io
#
# License: MIT
#
# ===========================================================


import os
import sys
import datetime
import json
import shutil

from validate import validate_input
from validate import validate_license
from validate import validate_template
from settings import Paths


def get_input():
	"""Get user input about the new repository."""
	
	# Get repo name.
	while True:
		repo_name = str(input("Repo name: "))

		args = {
			"required": True,
			"length": 30,
			"path_exists": True
		}

		validation_errors = validate_input(repo_name, args)
		if validation_errors == 0:
			break

	# Get repo description.
	while True:
		repo_description = str(input("Short description: "))

		args = {
			"required": False,
			"length": 60,
			"path_exists": False
		}

		validation_errors = validate_input(repo_description, args)
		if validation_errors == 0:
			break

	# Get repo license.
	while True:
		repo_license = input("License: ")

		args = {
			"required": False,
			"length": 30,
			"path_exists": False
		}

		validation_errors = validate_input(repo_license, args)
		template = validate_license(repo_license)
		
		if validation_errors == 0:
			if template["match"] == True:
				repo_license = template["template_file"]
				break

	# Get repo template.
	while True:
		repo_template = input("Template: ")

		args = {
			"required": False,
			"length": 30,
			"path_exists": False
		}

		validation_errors = validate_input(repo_template, args)
		template = validate_template(repo_template)
		
		if validation_errors == 0:
			if template["match"] == True:
				template_path = template["template_file"]
				break

	repo_dir = os.path.join(os.getcwd(), repo_name)
	
	repo_data = {
			"$repo_name": repo_name,
			"$repo_description": repo_description,
			"$repo_license": repo_license,
			"repo_dir": repo_dir,
			"repo_template": template_path,
	}

	return repo_data
	print("Repository data received...")


def copy_templates(source, dest, repo_license):
	"""Copy all template files and sub-directories to new repository directory."""
	
	try:
		shutil.copytree(source, dest)
		print("Templates copied to new repository...")
		
		if repo_license != "":
			try:
				shutil.copyfile(os.path.join(Paths.LICENSES, repo_license), os.path.join(dest, "LICENSE"))
				print("License generated...")
			except IsADirectoryError:
				print("Error: " + repo_license + " is a directory, not a template license file...")

	except FileExistsError:
		print("Error: " + repo_data["repo_dir"] + " already exists...")
		sys.exit()
	
	except NotADirectoryError:
		print("Error: " + repo_data["repo_template"] + " is not a directory...")
		sys.exit()


def rename_files(repo_data):
	"""
	Rename the main directory and main.py file with repository name.
	"""
	
	main_dir_name = repo_data["$repo_name"]
	main_py_name = repo_data["$repo_name"] + ".py"
	
	def rename_maindir():
		
		for root, subdirs, files in os.walk(repo_data["repo_dir"], topdown=True):
			for dirname in subdirs:
				if dirname == "main":
					path_old = os.path.join(root, dirname)
					path_new = os.path.join(root, main_dir_name)
					os.rename(path_old, path_new)
					print("main directory renamed to " + main_dir_name + "...")
		
	def rename_mainpy():
		
		for root, subdirs, files in os.walk(repo_data["repo_dir"], topdown=True):
			for filename in files:
				if filename == "main.py":
					path_old = os.path.join(root, filename)
					path_new = os.path.join(root, main_py_name)
					os.rename(path_old, path_new)
					print("main.py renamed to " + main_py_name + "...")

	rename_maindir()
	rename_mainpy()


def get_placeholders(settings_file):
	"""Get placeholders for filling repository data."""

	try:
		with open(settings_file, "r") as f:
			data = json.load(f)
			return data
			print("Placeholders pulled from user settings...")
	
	except ValueError:
		print("Error: Something went wrong in data/settings/placeholders.json file...")
		sys.exit()


def replace_placeholders(repo_data, placeholders):
	"""Iterate through repository files and replace placeholder values."""
	
	for root, subdirs, files in os.walk(repo_data["repo_dir"], topdown=True):
	
		for filename in files:
			
			filepath = os.path.join(root, filename)
			
			with open(filepath, "r") as input_file:
				content = input_file.read()
			
			for key, value in placeholders.items():
				content = content.replace(key, value)
			for key, value in repo_data.items():
				content = content.replace(key, value)
			
			content = content.replace("$year", str(datetime.date.today().year))

			with open(filepath, "w") as output_file:
				output_file.write(content)

	print("Placeholders replaced with user data...")


def main():
	"""Run the functions."""

	repo_data = get_input()
	
	placeholders = get_placeholders(Paths.USER_SETTINGS)

	copy_templates(repo_data["repo_template"], repo_data["repo_dir"], repo_data["$repo_license"])
	rename_files(repo_data)
	
	replace_placeholders(repo_data, placeholders)
	
	print("Successfully created the repository at: " + repo_data["repo_dir"])
	

if __name__ == "__main__":
	main()
