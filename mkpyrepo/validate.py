"""Module for validating user input."""


import os

from settings import Paths


def validate_input(user_input, args):
	"""
	Validate the user input. 
	- Check for allowed chars, field required or not, length.
	- Check if directory path already exists for the repo.
	"""
	
	allowed_chars = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9","-","_",".",","]
	
	validation_errors = 0

	# Validate characters.
	for char in user_input:
		if char not in allowed_chars:
			print("Invalid characters...")
			validation_errors =+ 1
			
	# Validate if field is required.
	if args["required"] == True:
		if user_input == "" or user_input == " ":
			print("Field cannot be empty...")
			validation_errors =+ 1
			
	# Validate length.
	if len(user_input) > args["length"]:
		print("Input is too long...")
		validation_errors =+ 1

	# Validate if directory path already exists.
	if args["path_exists"] == True:
		if os.path.isdir(user_input):
			print("Repository already exists in the same directory...")
			validation_errors =+ 1
		
	return validation_errors


def validate_license(user_input):
	"""
	Check if the template license user chooses exists.
	If file contains .txt or .md, catch that and output full filename.
	"""
	
	validation_data = {
		"match": "",
		"template_file": "",
	}
	
	if user_input != "":
		for licensefile in os.listdir(Paths.LICENSES):
			if os.path.isfile(os.path.join(Paths.LICENSES, licensefile)):
				if licensefile.endswith(".txt") or licensefile.endswith(".md"):
					license_template = licensefile.split(".")[0]
				else:
					license_template = licensefile
				if license_template == user_input:
					match_found = True
					validation_data.update({"match": match_found, "template_file": licensefile})
					break
				else:
					match_found = False
		if match_found == False:
			print("License not found. Try again...")
	else:
		match_found = True
		validation_data.update({"match": match_found, "template_file": ""})

	return validation_data


def validate_template(user_input):
	"""
	Check if the template structure user chooses exists.
	If template field is empty, output default template.
	"""
	
	validation_data = {
		"match": "",
		"template_file": "",
	}
	
	if user_input != "":
		for templatefile in os.listdir(Paths.TEMPLATES):
			if templatefile == user_input:
				match_found = True
				template_path = os.path.join(Paths.TEMPLATES, templatefile)
				validation_data.update({"match": match_found, "template_file": template_path})
				break
			else:
				match_found = False
				validation_data.update({"match": match_found})
		if match_found == False:
			print("Template not found. Try again...")
	else:
		match_found = True
		template_path = os.path.join(Paths.TEMPLATES, "default")
		validation_data.update({"match": match_found, "template_file": template_path})

	return validation_data

