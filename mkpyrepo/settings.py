"""Module for program settings."""


import os
from pathlib import Path


class Paths:
	
	APP_PATH = os.path.realpath(__file__)
	APP_DIR = Path(APP_PATH).parents[1]

	DATA_DIR = os.path.join(APP_DIR, "data")

	TEMPLATES = os.path.join(DATA_DIR, "templates")
	LICENSES = os.path.join(DATA_DIR, "licenses")
	SETTINGS = os.path.join(DATA_DIR, "settings")

	USER_SETTINGS = os.path.join(SETTINGS, "placeholders.json")
